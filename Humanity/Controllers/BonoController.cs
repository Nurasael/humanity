﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Humanity.DAL;
using Humanity.Models;

namespace Humanity.Controllers
{
    public class BonoController : Controller
    {
        private HumanityContext db = new HumanityContext();

        // GET: Bonoes
        public ActionResult Index()
        {
            return View(db.Bonos.ToList());
        }

        // GET: Bonoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bono bono = db.Bonos.Find(id);
            if (bono == null)
            {
                return HttpNotFound();
            }
            return View(bono);
        }

        // GET: Bonoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Bonoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Monto")] Bono bono)
        {
            if (ModelState.IsValid)
            {
                db.Bonos.Add(bono);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bono);
        }

        // GET: Bonoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bono bono = db.Bonos.Find(id);
            if (bono == null)
            {
                return HttpNotFound();
            }
            return View(bono);
        }

        // POST: Bonoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Monto")] Bono bono)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bono).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bono);
        }

        // GET: Bonoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bono bono = db.Bonos.Find(id);
            if (bono == null)
            {
                return HttpNotFound();
            }
            return View(bono);
        }

        // POST: Bonoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bono bono = db.Bonos.Find(id);
            db.Bonos.Remove(bono);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
