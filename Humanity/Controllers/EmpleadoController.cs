﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Humanity.DAL;
using Humanity.Models;

namespace Humanity.Controllers
{
    public class EmpleadoController : Controller
    {
        private HumanityContext db = new HumanityContext();

        // GET: Empleado
        public ActionResult Index()
        {
            var empleados = db.Empleados.Include(e => e.Afp).Include(e => e.Comuna).Include(e => e.CuentaBancaria).Include(e => e.PrevisionSalud);
            return View(empleados.ToList());
        }

        // GET: Empleado/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = db.Empleados.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        // GET: Empleado/Create
        public ActionResult Create()
        {
            ViewBag.AFPId = new SelectList(db.AFPs, "Id", "Nombre");
            ViewBag.ComunaId = new SelectList(db.Comunas, "Id", "Nombre");
            ViewBag.CuentaBancariaId = new SelectList(db.CuentasBancaria, "Id", "Cuenta");
            ViewBag.PrevisionSaludId = new SelectList(db.PrevionesSalud, "Id", "Nombre");
            return View();
        }

        // POST: Empleado/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Rut,Nombre,Apellido,Foto,Telefono,Direccion,Email,Nacionalidad,FechaNacimiento,ComunaId,CuentaBancariaId,AFPId,PrevisionSaludId")] Empleado empleado)
        {
            if (ModelState.IsValid)
            {
                db.Empleados.Add(empleado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AFPId = new SelectList(db.AFPs, "Id", "Nombre", empleado.AFPId);
            ViewBag.ComunaId = new SelectList(db.Comunas, "Id", "Nombre", empleado.ComunaId);
            ViewBag.CuentaBancariaId = new SelectList(db.CuentasBancaria, "Id", "Cuenta", empleado.CuentaBancariaId);
            ViewBag.PrevisionSaludId = new SelectList(db.PrevionesSalud, "Id", "Nombre", empleado.PrevisionSaludId);
            return View(empleado);
        }

        // GET: Empleado/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = db.Empleados.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            ViewBag.AFPId = new SelectList(db.AFPs, "Id", "Nombre", empleado.AFPId);
            ViewBag.ComunaId = new SelectList(db.Comunas, "Id", "Nombre", empleado.ComunaId);
            ViewBag.CuentaBancariaId = new SelectList(db.CuentasBancaria, "Id", "Cuenta", empleado.CuentaBancariaId);
            ViewBag.PrevisionSaludId = new SelectList(db.PrevionesSalud, "Id", "Nombre", empleado.PrevisionSaludId);
            return View(empleado);
        }

        // POST: Empleado/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Rut,Nombre,Apellido,Foto,Telefono,Direccion,Email,Nacionalidad,FechaNacimiento,ComunaId,CuentaBancariaId,AFPId,PrevisionSaludId")] Empleado empleado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empleado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AFPId = new SelectList(db.AFPs, "Id", "Nombre", empleado.AFPId);
            ViewBag.ComunaId = new SelectList(db.Comunas, "Id", "Nombre", empleado.ComunaId);
            ViewBag.CuentaBancariaId = new SelectList(db.CuentasBancaria, "Id", "Cuenta", empleado.CuentaBancariaId);
            ViewBag.PrevisionSaludId = new SelectList(db.PrevionesSalud, "Id", "Nombre", empleado.PrevisionSaludId);
            return View(empleado);
        }

        // GET: Empleado/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = db.Empleados.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        // POST: Empleado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Empleado empleado = db.Empleados.Find(id);
            db.Empleados.Remove(empleado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
