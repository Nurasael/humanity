﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EntityFramework.Triggers;
using Humanity.Models;

namespace Humanity.DAL
{
    public class HumanityContext : DbContext
    {
        public HumanityContext() : base("HumanityContext") {}

        public DbSet<AFP> AFPs { get; set; }
        public DbSet<AnexoContrato> AnexosContrato { get; set; }
        public DbSet<Banco> Bancos { get; set; }
        public DbSet<Bono> Bonos { get; set; }
        public DbSet<Cargo> Cargos { get; set; }
        public DbSet<Comuna> Comunas { get; set; }
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<CuentaBancaria> CuentasBancaria { get; set; }
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Fondo> Fondos { get; set; }
        public DbSet<GrupoUsuario> GruposUsuario { get; set; }
        public DbSet<HistorialEmpleado> HistorialEmpleados { get; set; }
        public DbSet<HoraExtra> HorasExtras { get; set; }
        public DbSet<Horario> Horarios { get; set; }
        public DbSet<Permiso> Permisos  { get; set; }
        public DbSet<PrevisionSalud> PrevionesSalud { get; set; }
        public DbSet<Privilegio> Privilegios { get; set; }
        public DbSet<RazonDespido> RazonDespidos { get; set; }
        public DbSet<Region> Regiones { get; set; }
        public DbSet<RegistroHorario> RegistrosHorarios { get; set; }

        public DbSet<Posicion> Posiciones { get; set; }
        public DbSet<RelSueldoBono> RelSueldoBonos { get; set; }

        public DbSet<SueldoBase> SueldosBases { get; set; }
        public DbSet<TipoContrato> TiposContrato { get; set; }
        public DbSet<TipoJornada> TiposJornada { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Fondo>()
                .HasMany(f => f.AFPs).WithMany(a => a.Fondos)
                .Map(t => t.MapLeftKey("FondoId")
                .MapRightKey("AFPId")
                .ToTable("FondoAFP"));

            modelBuilder.Entity<Empleado>()
                .HasMany(e => e.Contratos).WithMany(c => c.Empleados)
                .Map(t => t.MapLeftKey("EmpleadoId")
                .MapRightKey("ContratoId")
                .ToTable("EmpleadoContrato"));

            modelBuilder.Entity<Departamento>()
                .HasMany(d => d.Empleados).WithMany(e => e.Departamentos)
                .Map(t => t.MapLeftKey("DepartamentoId")
                .MapRightKey("EmpleadoId")
                .ToTable("DepartamentoEmpleado"));

        }

        public override int SaveChanges()
        {
            return this.SaveChangesWithTriggers(base.SaveChanges);
        }
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return this.SaveChangesWithTriggersAsync(base.SaveChangesAsync, cancellationToken);
        }
    }
}
