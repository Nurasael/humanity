﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Humanity.Startup))]
namespace Humanity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
