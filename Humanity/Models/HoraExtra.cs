﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanity.Models
{
    public class HoraExtra
    {
        public int Id { get; set; }
        [DataType(DataType.Duration)]
        public int Cantidad { get; set; }
        [DataType(DataType.Date)]
        public DateTime Fecha { get; set; }
    }
}
