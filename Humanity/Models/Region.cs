﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanity.Models
{
    public class Region
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; } 
        public virtual ICollection<Comuna> Comunas { get; set; } 
    }
}
