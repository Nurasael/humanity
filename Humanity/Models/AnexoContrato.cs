﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class AnexoContrato
    {
        public int Id { get; set; }
        [Required]
        public string Descripcion { get; set; }

        public virtual ICollection<Contrato> Contratos { get; set; } 
    }
}