﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class Horario
    {
        public int Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime HorarioEntrada { get; set; }
        [DataType(DataType.Date)]
        public DateTime HorarioSalida { get; set; }
        public int TipoJornadaId { get; set; }

        public virtual TipoJornada TipoJornada { get; set; }
        public virtual ICollection<RegistroHorario> RegistrosHorario { get; set; }
    }
}