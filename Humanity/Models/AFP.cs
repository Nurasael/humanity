﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class AFP
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P2}")]
        public double Descuento { get; set; }

        public double Comision { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }
        public virtual ICollection<Fondo> Fondos { get; set; }
    }
}