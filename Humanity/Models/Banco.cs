﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Humanity.Models
{
    public class Banco
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nombre Banco")]
        public string Nombre { get; set; }

        public virtual ICollection<CuentaBancaria> CuentasBancarias { get; set; }
    }
}
