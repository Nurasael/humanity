﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using EntityFramework.Triggers;
using Humanity.DAL;

namespace Humanity.Models
{
    public class Contrato : ITriggerable
    {
        public int Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime Inicio { get; set; }
        [DataType(DataType.Date)]
        public DateTime Termino { get; set; }

        public int Estado { get; set; }

        public int TipoContratoId { get; set; }
        public int AnexoContratoId { get; set; }
        public int RelJornadaCargoId { get; set; }

        public virtual TipoContrato TipoContrato { get; set; }
        public virtual AnexoContrato AnexoContrato { get; set; }
        public virtual Posicion Posicion { get; set; }

        public virtual ICollection<SueldoBase> SueldosBase { get; set; }
        public virtual ICollection<RazonDespido> RazonesPorDespido { get; set; }
        public virtual ICollection<Empleado> Empleados { get; set; }

        public Contrato()
        {
            this.Triggers().Deleting += entry =>
            {
                using (var context = new HumanityContext())
                {
                    string[] data = context.Database.SqlQuery<string[]>(
                        "SELECT " + 
                        "   Empleado.Rut as Rut," + 
                        "   Empleado.Nombre as Nombre," +
                        "   Empleado.Apellido as Apellido," + 
                        "   c.Cargo as Cargo" + 
                        "FROM Empleado" +
                        "   INNER JOIN RelEmpleadoContrato ON (Empleado.Id = RelEmpleadoContrato.EmpleadoId)" +
                        "   INNER JOIN Contrato ON (Contrato.Id = RelEmpleadoContrato.ContratoId)" +
                        "   INNER JOIN Cargo as c ON (c.Id = Contrato.CargoId)" + 
                        string.Format("WHERE RelEmpleadoContrato = {0}", Id)
                        ).ToList()[0];
                    var historial = new HistorialEmpleado { Rut = data[0], Nombre = data[1], Apellido = data[2], Cargo = data[3]};
                    context.HistorialEmpleados.Add(historial);
                    context.SaveChanges();
                }
            };
        }
    }
}
