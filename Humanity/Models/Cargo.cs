﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class Cargo
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        public int TipoContratoId { get; set; }

        public TipoContrato TipoContrato { get; set; }
        public virtual ICollection<Posicion> Posiciones { get; set; } 
    }
}
