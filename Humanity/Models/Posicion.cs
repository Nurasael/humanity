﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class Posicion
    {
        public int Id { get; set; }
        [DataType(DataType.Currency)]
        public int Sueldo { get; set; }
        public int TipoJornadaId { get; set; }
        public int CargoId { get; set; }

        public virtual Cargo Cargo { get; set; }
        public virtual TipoJornada TipoJornada { get; set; }
        public virtual ICollection<Contrato> Contratos { get; set; }
        
    }
}