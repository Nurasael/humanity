﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanity.Models
{
    public class TipoJornada
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [DataType(DataType.Duration)]
        public int HorasSemanales { get; set; }

        public virtual ICollection<Posicion> Posiciones { get; set; } 
    }
}
