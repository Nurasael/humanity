﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanity.Models
{
    public class Departamento
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }
        [Display(Name = "Jefe")]
        public int? EmpleadoId { get; set; }

        public virtual Empleado Jefe { get; set; }
        public virtual ICollection<SueldoBase> SueldosBase { get; set; } 
        public virtual ICollection<Empleado> Empleados { get; set; } 
    }
}
