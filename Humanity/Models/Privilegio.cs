﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanity.Models
{
    public class Privilegio
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }

        public virtual ICollection<Permiso> Permisos { get; set; } 
    }
}
