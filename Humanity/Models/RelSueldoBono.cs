﻿using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class RelSueldoBono
    {
        public int Id { get; set; }

        [DataType(DataType.Currency)]
        public int Total { get; set; }

        public int BonoId { get; set; }
        public int SueldoBaseId { get; set; }

        public virtual Bono Bono { get; set; }
        public virtual SueldoBase SueldoBase { get; set; }
    }
}