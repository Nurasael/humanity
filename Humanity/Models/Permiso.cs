﻿namespace Humanity.Models
{
    public class Permiso
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public int PrivilegioId { get; set; }
        public int GrupoUsuarioId { get; set; }
        
        public virtual Privilegio Privilegio { get; set; }
        public virtual GrupoUsuario GrupoUsuario { get; set; }
    }
}
