﻿using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class RazonDespido
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Descripcion { get; set; }

        public int ContratoId { get; set; }

        public virtual Contrato Contrato { get; set; }
    }
}