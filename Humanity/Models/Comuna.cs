﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanity.Models
{
    public class Comuna
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int RegionId { get; set; }

        public virtual Region Region { get; set; }
    }
}
