﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanity.Models
{
    public class PrevisionSalud
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P2}")]
        public double Descuento { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; } 
    }
}
