﻿using System.Collections;
using System.Collections.Generic;

namespace Humanity.Models
{
    public class CuentaBancaria
    {
        public int Id { get; set; }
        public int BancoId { get; set; }
        public string Cuenta { get; set; }

        public virtual Banco Banco { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }
    }
}