﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EntityFramework.Triggers;

namespace Humanity.Models
{
    public class Empleado
    {
        public int Id { get; set; }
        [Required]
        public string Rut { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Apellido { get; set; }
        [DataType(DataType.Upload)]
        public byte[] Foto { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }
        [Required]
        public string Direccion { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string Nacionalidad { get; set; }
        [Required]
        [DataType(DataType.Date), Display(Name = "Fecha Nacimiento")]
        public DateTime FechaNacimiento { get; set; }

        [Display(Name = "Comuna")]
        public int ComunaId { get; set; }
        [Display(Name = "Cuenta Bancaria")]
        public int CuentaBancariaId { get; set; }
        [Display(Name = "AFP")]
        public int AFPId { get; set; }
        [Display(Name = "Prevision de Salud")]
        public int PrevisionSaludId { get; set; }

        public virtual Comuna Comuna { get; set; }
        public virtual CuentaBancaria CuentaBancaria { get; set; }
        public virtual AFP Afp { get; set; }
        public virtual PrevisionSalud PrevisionSalud { get; set; }

        public virtual ICollection<Departamento> Departamentos { get; set; }
        public virtual ICollection<RegistroHorario> RegistrosHorario { get; set; }
        public virtual ICollection<Contrato> Contratos { get; set; }
    }
}