﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanity.Models
{
    public class Fondo
    {
        public int Id { get; set; }

        [StringLength(45)]
        public string Nombre { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P2}")]
        [DefaultValue(1.2)]
        public double Descuento { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P2}")]
        [DefaultValue(1.2)]
        public double Alza { get; set; }

        public virtual ICollection<AFP> AFPs { get; set; }
    }
}
