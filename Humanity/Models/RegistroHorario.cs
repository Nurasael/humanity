﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class RegistroHorario
    {
        public int Id { get; set; }
        [DataType(DataType.Time)]
        public DateTime Llegada { get; set; }
        [DataType(DataType.Time)]
        public DateTime Salida { get; set; }
        [DataType(DataType.Date)]
        public DateTime Fecha { get; set; }

        public int EmpleadoId { get; set; }
        public int HorarioId { get; set; }

        public virtual Empleado Empleado { get; set; }
        public virtual Horario Horario { get; set; }
    }
}