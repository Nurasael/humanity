﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class Bono
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [DataType(DataType.Currency)]
        public int Monto { get; set; }

        public virtual ICollection<RelSueldoBono> RelSueldoBonos { get; set; } 
    }
}