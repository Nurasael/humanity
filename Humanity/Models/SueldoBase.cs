﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Humanity.Models
{
    public class SueldoBase
    {
        public int Id { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P2}")]
        public double DescuentoAFP { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P2}")]
        public double DescuentoPSalud { get; set; }

        public int DepartamentoId { get; set; }
        public int ContratoId { get; set; }

        public virtual Departamento Departmento { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual ICollection<RelSueldoBono> RelSueldoBonos { get; set; } 
    }
}